class AddTokenToActor < ActiveRecord::Migration[5.2]
  def change
    add_column :actors, :token, :string
  end
end
