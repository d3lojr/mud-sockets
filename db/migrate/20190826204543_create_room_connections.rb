class CreateRoomConnections < ActiveRecord::Migration[5.2]
  def change
    create_table :room_connections do |t|
      t.references :room_from
      t.references :room_to
      t.text :command

      t.timestamps
    end
  end
end
