class ModifyRooms < ActiveRecord::Migration[5.2]
  def up
    drop_table :room_messages
    remove_index :rooms, :name

    add_column :rooms, :description, :text
  end

  def down
    create_table :room_messages do |t|
      t.references :room, foreign_key: true
      t.references :user, foreign_key: true
      t.text :message

      t.timestamps
    end

    add_index :rooms, :name, unique: true

    remove_column :rooms, :description
  end
end
