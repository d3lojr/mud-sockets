class CreateAvatars < ActiveRecord::Migration[5.2]
  def change
    create_table :avatars do |t|
      t.string :name
      t.references :user
      t.references :job

      t.timestamps
    end
    add_index :avatars, :name, unique: true
  end
end
