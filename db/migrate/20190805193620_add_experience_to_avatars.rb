class AddExperienceToAvatars < ActiveRecord::Migration[5.2]
  def change
    add_column :avatars, :experience, :bigint
    add_column :avatars, :level, :integer
  end
end
