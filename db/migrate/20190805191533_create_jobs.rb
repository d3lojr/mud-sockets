class CreateJobs < ActiveRecord::Migration[5.2]
  def up
    create_table :jobs do |t|
      t.string :name

      t.timestamps
    end
    add_index :jobs, :name, unique: true

    Job.create([
      {name: 'Warrior'},
      {name: 'Mage'},
      {name: 'Cleric'}
    ])
  end

  def down
    drop_table :jobs
  end
end
