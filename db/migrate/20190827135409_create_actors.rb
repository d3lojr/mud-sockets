class CreateActors < ActiveRecord::Migration[5.2]
  def change
    create_table :actors do |t|
      t.integer :room_id
      t.integer :global_actor_id
      t.string :global_actor_type

      t.timestamps
    end
  end
end
