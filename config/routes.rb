Rails.application.routes.draw do
  resources :jobs
  resources :avatars do
    get :login
  end
  devise_for :users

  root controller: :avatars, action: :index

  get 'lobby', controller: :lobby, action: :index
  post 'lobby/message', controller: :lobby, action: :message
end
