class Room < ApplicationRecord
  has_many :outgoing_connections,
           class_name: 'RoomConnection',
           join_table: :room_connections,
           foreign_key: :room_from_id

  has_many :incoming_connections,
           class_name: 'RoomConnection',
           join_table: :room_connections,
           foreign_key: :room_to_id

  has_many :actors, class_name: 'Actor'

  def introduction
    other_players = actors.where(global_actor_type: 'Avatar').size - 1

    if other_players.positive?
      response = description + "<br /> There #{other_players > 1 ? 'are' : 'is'} #{other_players.humanize} other player#{other_players > 1 ? 's' : ''} in the room."
    else
      response = description
    end

    if outgoing_connections.present?
      response += "<br /> You see #{outgoing_connections.size} exit(s): #{outgoing_connections.collect{|c| c.command}.join(', ')}"
    end

    response
  end
end
