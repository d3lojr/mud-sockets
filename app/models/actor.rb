class Actor < ApplicationRecord
  belongs_to :room, class_name: 'Room'
  belongs_to :global_actor, polymorphic: true

  validates_presence_of :room, :token
  validates :token, uniqueness: true
end
