class Monster < ApplicationRecord
  has_many :actors, as: :global_actor, dependent: :destroy
end
