class RoomConnection < ApplicationRecord
  belongs_to :room_from, class_name: 'Room'
  belongs_to :room_to, class_name: 'Room'
end
