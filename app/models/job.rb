class Job < ApplicationRecord
  has_many :avatars, class_name: 'Avatar'
end
