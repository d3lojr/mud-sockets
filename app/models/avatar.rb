class Avatar < ApplicationRecord
  belongs_to :user
  belongs_to :job, class_name: 'Job'

  validates_presence_of :user, :job, :level, :experience

  has_one :actor, as: :global_actor, dependent: :destroy
end
