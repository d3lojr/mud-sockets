class RoomChannel < ApplicationCable::Channel
  def subscribed
    stream_for params[:room]

    # or
    # stream_from "room_#{params[:room]}"
  end
end