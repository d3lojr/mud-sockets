class LobbyController < ApplicationController
  before_action :ensure_actor
  skip_before_action :verify_authenticity_token

  def index
    @actor = Actor.find(session[:actor_id])
  end

  def message
    @message = params.dig(:message)
    @actor = Actor.find_by(token: params.dig(:room_id))

    @actor.room.actors.where(global_actor_type: 'Avatar').each do |actor|
      RoomChannel.broadcast_to actor.token, {
        message: @message, username: @actor.global_actor.name,
        updated_at: Time.now.strftime('%H:%M:%S')
      }
    end

    render json: { success: true }
  end

  protected

  def ensure_actor
    if session[:actor_id].blank?
      redirect_to '/avatars', flash: { danger: 'Please select an avatar.' }
    end
  end
end
