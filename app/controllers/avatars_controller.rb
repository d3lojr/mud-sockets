class AvatarsController < ApplicationController
  def index
    @avatars = current_user.avatars
  end

  def new
    @avatar = Avatar.new
  end

  def show
    @avatar = Avatar.find(params[:id])
  end

  def create
    @avatar = Avatar.new permitted_parameters
    @avatar.user_id = current_user.id

    @avatar.level = 1
    @avatar.experience = 0

    if @avatar.save
      flash[:success] = "Avatar #{@avatar.name} was created successfully"
      redirect_to avatars_path
    else
      render :new
    end
  end

  def login
    @avatar = Avatar.find(params[:avatar_id])

    actor = Actor.find_by(global_actor_id: @avatar.id, global_actor_type: 'Avatar')
    if actor.nil?
      actor = Actor.create(
        room_id: 1,
        global_actor_id: @avatar.id,
        token: SecureRandom.hex(10),
        global_actor_type: 'Avatar'
      )
    end

    session[:actor_id] = actor.id

    redirect_to '/lobby'
  end

  protected

  def permitted_parameters
    params.require(:avatar).permit(:name, :job_id)
  end
end
