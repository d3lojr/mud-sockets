$(function() {
  // $('#new_room_message').on('ajax:success', function(a, b,c ) {
  //   $(this).find('input[type="text"]').val('');
  // });
  $('body').on('click', 'input.chat-input[type="submit"]', function() {
    var message = $('input.chat-input[type="text"]').val();
    var room_id = $('.chat').data('room-id');
    var authToken = $('input[name="authenticity_token"]').val();
    $.ajax({
      url: '/lobby/message',
      type: 'POST',
      dataType: 'json',
      data: {
        room_id: room_id,
        message: message
      },
      success: function(response) {
        $('input.chat-input[type="text"]').val('');
      }
    });
  });
});